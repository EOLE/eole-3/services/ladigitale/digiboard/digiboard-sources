import axios from 'axios'

export { onBeforeRender }

async function onBeforeRender (pageContext) {
	let pageProps, erreur
	const tableau = pageContext.routeParams.tableau
	const reponse = await axios.post(pageContext.hote + '/api/recuperer-donnees-tableau', {
		tableau: tableau
	}, {
		headers: { 'Content-Type': 'application/json' }
	}).catch(function () {
		erreur = true
		pageProps = { erreur }
	})
	if (!reponse || !reponse.hasOwnProperty('data') || (reponse.data && reponse.data === 'erreur')) {
		erreur = true
		pageProps = { erreur }
	} else {
		if (reponse.data.hasOwnProperty('statut') === true && reponse.data.statut === 'ferme') {
			reponse.data.options.edition = 'fermee'
		}
		const params = pageContext.params
		const hote = pageContext.hote
		const userAgent = pageContext.userAgent
		const langues = pageContext.langues
		const identifiant = pageContext.identifiant
		const nom = pageContext.nom
		const langue = pageContext.langue
		const role = pageContext.role
		const tableaux = pageContext.tableaux
		const digidrive = pageContext.digidrive
		const titre = reponse.data.titre
		const donnees = reponse.data.donnees
		const options = reponse.data.options
		const commentaires = reponse.data.commentaires
		const vues = reponse.data.vues
		const titrePage = titre + ' - Digiboard by La Digitale'
		pageProps = { params, hote, userAgent, langues, identifiant, nom, langue, role, tableaux, digidrive, tableau, titre, donnees, options, commentaires, vues, titrePage }
	}
	return {
		pageContext: {
			pageProps
		}
	}
}
