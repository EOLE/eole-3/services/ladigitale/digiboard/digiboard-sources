import 'dotenv/config'
import path from 'path'
import fs from 'fs-extra'
import express from 'express'
import { createServer } from 'http'
import { Server } from 'socket.io'
import { createAdapter } from '@socket.io/cluster-adapter'
import compression from 'compression'
import axios from 'axios'
import cors from 'cors'
import { createClient } from 'redis'
import bodyParser from 'body-parser'
import helmet from 'helmet'
import v from 'voca'
import multer from 'multer'
import sharp from 'sharp'
import archiver from 'archiver'
import extract from 'extract-zip'
import dayjs from 'dayjs'
import bcrypt from 'bcrypt'
import cron from 'node-cron'
import { fileURLToPath } from 'url'
import RedisStore from 'connect-redis'
import session from 'express-session'
import base64 from 'base-64'
import { renderPage } from 'vike/server'

const production = process.env.NODE_ENV === 'production'
const cluster = parseInt(process.env.NODE_CLUSTER) === 1
const __dirname = path.dirname(fileURLToPath(import.meta.url))
const root = `${__dirname}/..`

demarrerServeur()

async function demarrerServeur () {
	const app = express()
	app.use(compression())
	const httpServer = createServer(app)

	let hote = 'http://localhost:3000'
	if (production) {
		hote = process.env.DOMAIN
	} else if (process.env.PORT) {
		hote = 'http://localhost:' + process.env.PORT
	}
	let db
	let db_port = 6379
	if (process.env.DB_PORT) {
		db_port = process.env.DB_PORT
	}
	if (production) {
		db = await createClient({
			url: 'redis://default:' + process.env.DB_PWD  + '@' + process.env.DB_HOST + ':' + db_port
		}).on('error', function (err) {
			console.log('redis: ', err)
		}).connect()
	} else {
		db = await createClient({
			url: 'redis://localhost:' + db_port
		}).on('error', function (err) {
			console.log('redis: ' + err)
		}).connect()
	}
	let storeOptions, cookie, dureeSession, domainesAutorises
	if (production) {
		storeOptions = {
			host: process.env.DB_HOST,
			port: db_port,
			pass: process.env.DB_PWD,
			client: db,
			prefix: 'sessions:'
		}
		cookie = {
			sameSite: 'None',
			secure: true
		}
	} else {
		storeOptions = {
			host: 'localhost',
			port: db_port,
			client: db,
			prefix: 'sessions:'
		}
		cookie = {
			secure: false
		}
	}
	const redisStore = new RedisStore(storeOptions)
	const sessionOptions = {
		secret: process.env.SESSION_KEY,
		store: redisStore,
		name: 'digiboard',
		resave: false,
		rolling: true,
		saveUninitialized: false,
		cookie: cookie
	}
	if (process.env.SESSION_DURATION) {
		dureeSession = parseInt(process.env.SESSION_DURATION)
	} else {
		dureeSession = 864000000 //3600 * 24 * 10 * 1000
	}
	const sessionMiddleware = session(sessionOptions)

	if (production && process.env.AUTHORIZED_DOMAINS) {
		domainesAutorises = process.env.AUTHORIZED_DOMAINS.split(',')
	} else {
		domainesAutorises = '*'
	}

	let earlyHints103 = false
	if (process.env.EARLY_HINTS && parseInt(process.env.EARLY_HINTS) === 1) {
		earlyHints103 = true
	}

	cron.schedule('59 23 * * Saturday', async function () {
		await fs.emptyDir(path.join(__dirname, '..', '/static/temp'))
	})

	app.set('trust proxy', true)
	app.use(
		helmet.contentSecurityPolicy({
			directives: {
				"default-src": ["'self'", "https:", "ws:", "data:"],
				"script-src": ["'self'", "'unsafe-inline'", "'unsafe-eval'"],
				"img-src": ["'self'", "https:", "data:"],
				"media-src": ["'self'", "data:"],
				"frame-ancestors": ["*"]
			}
		})
	)
	// app.use(helmet.frameguard(false))
	app.use(bodyParser.json({ limit: '50mb' }))
	app.use(sessionMiddleware)
	app.use(cors({ 'origin': domainesAutorises }))
	if (parseInt(process.env.REVERSE_PROXY) !== 1 || !production) {
		app.use('/', express.static('static'))
	}

	if (!production) {
		const vite = await import('vite')
    	const viteDevMiddleware = (
      		await vite.createServer({
        		root,
        		server: { middlewareMode: true }
			})
    	).middlewares
    	app.use(viteDevMiddleware)
  	} else if (production && parseInt(process.env.REVERSE_PROXY) !== 1) {
		const sirv = (await import('sirv')).default
		app.use(sirv(`${root}/dist/client`))
	}

	app.get('/', async function (req, res, next) {
		let langue = 'fr'
		if (req.session.hasOwnProperty('langue') && req.session.langue !== '') {
			langue = req.session.langue
		}
		const pageContextInit = {
			urlOriginal: req.originalUrl,
			params: req.query,
			hote: hote,
			langues: ['fr', 'it', 'en'],
			langue: langue
		}
		const pageContext = await renderPage(pageContextInit)
		const { httpResponse } = pageContext
		if (!httpResponse) {
			return next()
		}
		const { body, statusCode, headers, earlyHints } = httpResponse
		if (earlyHints103 === true && res.writeEarlyHints) {
			res.writeEarlyHints({ link: earlyHints.map((e) => e.earlyHintLink) })
		}
		if (headers) {
			headers.forEach(([name, value]) => res.setHeader(name, value))
		}
		res.status(statusCode).send(body)
  	})

	app.get('/b/:tableau', async function (req, res, next) {
		let langue = 'fr'
		if (req.session.hasOwnProperty('langue') && req.session.langue !== '') {
			langue = req.session.langue
		}
		const userAgent = req.headers['user-agent']
		if (req.query.q && req.query.q !== '' && req.query.r && req.query.r !== '') {
			await verifierAcces(req, req.params.tableau, base64.decode(req.query.q), base64.decode(req.query.r))
		}
		if (req.session.identifiant === undefined || req.session.identifiant === '')  {
			const identifiant = 'u' + Math.random().toString(16).slice(3)
			req.session.identifiant = identifiant
			req.session.nom = genererPseudo()
			req.session.langue = langue
			req.session.role = 'participant'
			req.session.tableaux = []
			req.session.digidrive = []
			req.session.cookie.expires = new Date(Date.now() + dureeSession)
		}
		if (!req.session.hasOwnProperty('tableaux')) {
			req.session.pads = []
		}
		if (!req.session.hasOwnProperty('digidrive')) {
			req.session.digidrive = []
		}
		const pageContextInit = {
			urlOriginal: req.originalUrl,
			params: req.query,
			hote: hote,
			userAgent: userAgent,
			langues: ['fr', 'it', 'en'],
			identifiant: req.session.identifiant,
			nom: req.session.nom,
			langue: req.session.langue,
			role: req.session.role,
			tableaux: req.session.tableaux,
			digidrive: req.session.digidrive
		}
		const pageContext = await renderPage(pageContextInit)
		const { httpResponse } = pageContext
		if (!httpResponse) {
			return next()
		}
		const { body, statusCode, headers, earlyHints } = httpResponse
		if (earlyHints103 === true && res.writeEarlyHints) {
			res.writeEarlyHints({ link: earlyHints.map((e) => e.earlyHintLink) })
		}
		if (headers) {
			headers.forEach(([name, value]) => res.setHeader(name, value))
		}
		res.status(statusCode).send(body)
  	})
	
	app.post('/api/creer-tableau', async function (req, res) {
		if (req.session.identifiant === '' || req.session.identifiant === undefined) {
			const identifiant = 'u' + Math.random().toString(16).slice(3)
			req.session.identifiant = identifiant
		}
		if (!req.session.hasOwnProperty('tableaux')) {
			req.session.tableaux = []
		}
		if (!req.session.hasOwnProperty('digidrive')) {
			req.session.digidrive = []
		}
		const titre = req.body.titre
		const question = req.body.question
		const reponse = req.body.reponse
		const hash = await bcrypt.hash(reponse, 10)
		const tableau = Math.random().toString(16).slice(2)
		const date = dayjs().format()
		const resultat = await db.EXISTS('tableaux:' + tableau)
		if (resultat === null) { res.send('erreur') }
		if (resultat === 0) {
			const donnees = []
			donnees[0] = []
			const options = {}
			options.fond = '#ffffff'
			options.edition = 'ouverte'
			options.affichageAuteur = 'non'
			const commentaires = {}
			await db.HSET('tableaux:' + tableau, ['titre', titre, 'question', question, 'reponse', hash, 'donnees', JSON.stringify(donnees), 'options', JSON.stringify(options), 'commentaires', JSON.stringify(commentaires), 'date', date, 'vues', 0, 'derniereVisite', date])
			const chemin = path.join(__dirname, '..', '/static/fichiers/' + tableau)
			await fs.mkdirs(chemin)
			if (req.session.nom === '' || req.session.nom === undefined) {
				req.session.nom = genererPseudo()
			}
			if (req.session.langue === '' || req.session.langue === undefined) {
				req.session.langue = 'fr'
			}
			req.session.role = 'auteur'
			req.session.tableaux.push(tableau)
			req.session.cookie.expires = new Date(Date.now() + dureeSession)
			res.send(tableau)
		} else {
			res.send('existe_deja')
		}
	})
	
	app.post('/api/recuperer-donnees-tableau', async function (req, res) {
		const tableau = req.body.tableau
		const reponse = await db.EXISTS('tableaux:' + tableau)
		if (reponse === null) { res.send('erreur'); return false }
		if (reponse === 1) {
			let resultat = await db.HGETALL('tableaux:' + tableau)
			resultat = Object.assign({}, resultat)
			if (resultat === null) { res.send('erreur'); return false }
			let commentaires = {}
			if (resultat.hasOwnProperty('commentaires') === true) {
				commentaires = JSON.parse(resultat.commentaires)
			}
			let vues = 0
			if (resultat.hasOwnProperty('vues')) {
				vues = parseInt(resultat.vues)
			}
			vues = vues + 1
			const date = dayjs().format()
			await db.HSET('tableaux:' + tableau, ['vues', vues, 'derniereVisite', date])
			if (resultat.hasOwnProperty('statut') === true) {
				const titre = resultat.titre
				const statut = resultat.statut
				const donnees = JSON.parse(resultat.donnees)
				const options = JSON.parse(resultat.options)
				await db.HDEL('tableaux:' + tableau, 'statut')
				res.json({ titre: titre, statut: statut, donnees: donnees, options: options, commentaires: commentaires, vues: vues })
			} else {
				const titre = resultat.titre
				const donnees = JSON.parse(resultat.donnees)
				const options = JSON.parse(resultat.options)
				res.json({ titre: titre, donnees: donnees, options: options, commentaires: commentaires, vues: vues })
			}
		} else {
			res.send('erreur')
		}
	})
	
	app.post('/api/debloquer-tableau', async function (req, res) {
		const tableau = req.body.tableau
		const question = req.body.question
		const reponse = req.body.reponse
		let resultat = await db.HGETALL('tableaux:' + tableau)
		resultat = Object.assign({}, resultat)
		if (resultat === null || !resultat.hasOwnProperty('question') || !resultat.hasOwnProperty('reponse')) { res.send('erreur'); return false }
		if (resultat.question === question && await bcrypt.compare(reponse, resultat.reponse)) {
			req.session.role = 'auteur'
			if (!req.session.hasOwnProperty('tableaux')) {
				req.session.tableaux = []
			}
			if (!req.session.tableaux.includes(tableau)) {
				req.session.tableaux.push(tableau)
			}
			if (req.body.type === 'api') {
				if (!req.session.hasOwnProperty('digidrive')) {
					req.session.digidrive = []
				}
				req.session.digidrive.push(tableau)
				res.json({ message: 'tableau_debloque', digidrive: req.session.digidrive })
			} else {
				res.send('tableau_debloque')
			}
		} else {
			res.send('informations_incorrectes')
		}
	})
	
	app.post('/api/modifier-titre', async function (req, res) {
		const tableau = req.body.tableau
		if (req.session.role === 'auteur' && req.session.tableaux.includes(tableau)) {
			const titre = req.body.nouveautitre
			const resultat = await db.EXISTS('tableaux:' + tableau)
			if (resultat === null) { res.send('erreur'); return false }
			if (resultat === 1) {
				await db.HSET('tableaux:' + tableau, 'titre', titre)
				res.send('titre_modifie')
			} else {
				res.send('erreur')
			}
		} else {
			res.send('non_autorise')
		}
	})
	
	app.post('/api/modifier-acces', async function (req, res) {
		const tableau = req.body.tableau
		if (req.session.role === 'auteur' && req.session.tableaux.includes(tableau)) {
			const question = req.body.question
			const reponse = req.body.reponse
			const rep = await db.EXISTS('tableaux:' + tableau)
			if (rep === null) { res.send('erreur'); return false }
			if (rep === 1) {
				let resultat = await db.HGETALL('tableaux:' + tableau)
				resultat = Object.assign({}, resultat)
				if (resultat === null) { res.send('erreur'); return false }
				if (resultat.question === question && await bcrypt.compare(reponse, resultat.reponse)) {
					const nouvellequestion = req.body.nouvellequestion
					const nouvellereponse = req.body.nouvellereponse
					const hash = await bcrypt.hash(nouvellereponse, 10)
					await db.HSET('tableaux:' + tableau, ['question', nouvellequestion, 'reponse', hash])
					res.send('acces_modifie')
				} else {
					res.send('informations_incorrectes')
				}
			} else {
				res.send('erreur')
			}
		} else {
			res.send('non_autorise')
		}
	})
	
	app.post('/api/modifier-fond', async function (req, res) {
		const tableau = req.body.tableau
		if (req.session.role === 'auteur' && req.session.tableaux.includes(tableau)) {
			const fond = req.body.fond
			const resultat = await db.EXISTS('tableaux:' + tableau)
			if (resultat === null) { res.send('erreur'); return false }
			if (resultat === 1) {
				let reponse = await db.HGETALL('tableaux:' + tableau)
				reponse = Object.assign({}, reponse)
				if (reponse === null) { res.send('erreur'); return false }
				const options = JSON.parse(reponse.options)
				options.fond = fond
				await db.HSET('tableaux:' + tableau, 'options', JSON.stringify(options))
				res.send('fond_modifie')
			}
		} else {
			res.send('non_autorise')
		}
	})
	
	app.post('/api/exporter-tableau', async function (req, res) {
		const tableau = req.body.tableau
		if (req.session.role === 'auteur' && req.session.tableaux.includes(tableau)) {
			const items = req.body.items
			const commentaires = req.body.commentaires
			const fond = req.body.fond
			const chemin = path.join(__dirname, '..', '/static/temp')
			await fs.mkdirp(path.normalize(chemin + '/' + tableau))
			await fs.mkdirp(path.normalize(chemin + '/' + tableau + '/fichiers'))
			await fs.writeFile(path.normalize(chemin + '/' + tableau + '/donnees.json'), JSON.stringify({ items: items, commentaires: commentaires, fond: fond }, '', 4), 'utf8')
			for (let i = 0; i < items.length; i++) {
				for (let j = 0; j < items[i].length; j++) {
					if (Object.keys(items[i][j]).length > 0 && items[i][j].objet === 'image' && items[i][j].fichier !== '' && await fs.pathExists(path.join(__dirname, '..', '/static/fichiers/' + tableau + '/' + items[i][j].fichier))) {
						await fs.copy(path.join(__dirname, '..', '/static/fichiers/' + tableau + '/' + items[i][j].fichier), path.normalize(chemin + '/' + tableau + '/fichiers/' + items[i][j].fichier, { overwrite: true }))
					}
				}
			}
			const archiveId = Math.floor((Math.random() * 100000) + 1)
			const sortie = fs.createWriteStream(path.normalize(chemin + '/tableau-' + tableau + '_' + archiveId + '.zip'))
			const archive = archiver('zip', {
				zlib: { level: 9 }
			})
			sortie.on('finish', async function () {
				await fs.remove(path.normalize(chemin + '/' + tableau))
				res.send('tableau-' + tableau + '_' + archiveId + '.zip')
			})
			archive.pipe(sortie)
			archive.directory(path.normalize(chemin + '/' + tableau), false)
			archive.finalize()
		} else {
			res.send('non_autorise')
		}
	})
	
	app.post('/api/importer-tableau', function (req, res) {
		televerserArchive(req, res, async function (err) {
			if (err) { res.send('erreur_import'); return false }
			try {
				const tableau = req.body.tableau
				if (req.session.role === 'auteur' && req.session.tableaux.includes(tableau)) {
					const chemin = path.join(__dirname, '..', '/static/fichiers/' + tableau)
					await fs.emptyDir(chemin)
					const source = path.join(__dirname, '..', '/static/temp/' + req.file.filename)
					const cible = path.join(__dirname, '..', '/static/temp/archive-' + Math.floor((Math.random() * 100000) + 1))
					await extract(source, { dir: cible })
					const donnees = await fs.readJson(path.normalize(cible + '/donnees.json'))
					const items = donnees.items
					for (let i = 0; i < items.length; i++) {
						for (let j = 0; j < items[i].length; j++) {
							if (items[i][j].objet === 'image' && items[i][j].fichier !== '' && await fs.pathExists(path.normalize(cible + '/fichiers/' + items[i][j].fichier))) {
								await fs.copy(path.normalize(cible + '/fichiers/' + items[i][j].fichier), path.normalize(chemin + '/' + items[i][j].fichier, { overwrite: true }))
							}
						}
					}
					let commentaires = []
					if (donnees.hasOwnProperty('commentaires') === true) {
						commentaires = donnees.commentaires
					}
					let fond = '#ffffff'
					if (donnees.hasOwnProperty('fond') === true) {
						fond = donnees.fond
					}
					let reponse = await db.HGETALL('tableaux:' + tableau)
					reponse = Object.assign({}, reponse)
					if (reponse === null) { res.send('erreur_import'); return false }
					const options = JSON.parse(reponse.options)
					options.fond = fond
					await db.HSET('tableaux:' + tableau, ['donnees', JSON.stringify(items), 'commentaires', JSON.stringify(commentaires), 'options', JSON.stringify(options)])
					await fs.remove(source)
					await fs.remove(cible)
					res.json({ items: items, commentaires: commentaires, fond: fond })
				} else {
					res.send('non_autorise')
				}
			} catch (err) {
				await fs.remove(path.join(__dirname, '..', '/static/temp/' + req.file.filename))
				res.send('erreur_import')
			}
		})
	})
	
	app.post('/api/supprimer-tableau', async function (req, res) {
		const tableau = req.body.tableau
		if (req.session.role === 'auteur' && req.session.tableaux.includes(tableau)) {
			const reponse = await db.EXISTS('tableaux:' + tableau)
			if (reponse === null) { res.send('erreur'); return false }
			if (reponse === 1) {
				await db.DEL('tableaux:' + tableau)
				const chemin = path.join(__dirname, '..', '/static/fichiers/' + tableau)
				await fs.remove(chemin)
				res.send('tableau_supprime')
			} else {
				res.send('erreur')
			}
		} else {
			res.send('non_autorise')
		}
	})
	
	app.post('/api/terminer-session', function (req, res) {
		req.session.role = 'participant'
		req.session.tableaux = []
		res.send('session_terminee')
	})
	
	app.post('/api/modifier-nom', function (req, res) {
		const nom = req.body.nom
		req.session.nom = nom
		res.send('nom_modifie')
	})
	
	app.post('/api/modifier-langue', function (req, res) {
		if (req.session.identifiant === '' || req.session.identifiant === undefined) {
			const identifiant = 'u' + Math.random().toString(16).slice(3)
			req.session.identifiant = identifiant
		}
		if (!req.session.hasOwnProperty('tableaux')) {
			req.session.tableaux = []
		}
		const langue = req.body.langue
		req.session.langue = langue
		res.send('langue_modifiee')
	})
	
	app.post('/api/televerser-image', function (req, res) {
		televerser(req, res, function (err) {
			if (err) { res.send('erreur_televersement'); return false }
			const fichier = req.file
			const tableau = req.body.tableau
			let mimetype = fichier.mimetype
			const chemin = path.join(__dirname, '..', '/static/fichiers/' + tableau + '/' + fichier.filename)
			if (mimetype.split('/')[0] === 'image') {
				const extension = path.parse(fichier.filename).ext
				if (extension.toLowerCase() === '.jpg' || extension.toLowerCase() === '.jpeg') {
					sharp(chemin).withMetadata().rotate().jpeg().resize(1500, 1500, {
						fit: sharp.fit.inside,
						withoutEnlargement: true
					}).toBuffer(async function (err, buffer) {
						if (err) { res.send('erreur_televersement'); return false }
						await fs.writeFile(chemin, buffer)
						res.send(fichier.filename)
					})
				} else {
					sharp(chemin).withMetadata().resize(1500, 1500, {
						fit: sharp.fit.inside,
						withoutEnlargement: true
					}).toBuffer(async function (err, buffer) {
						if (err) { res.send('erreur_televersement'); return false }
						await fs.writeFile(chemin, buffer)
						res.send(fichier.filename)
					})
				}
			} else {
				res.send('erreur_type_fichier')
			}
		})
	})
	
	app.post('/api/copier-image', async function (req, res) {
		const image = req.body.image
		const tableau = req.body.tableau
		const info = path.parse(image)
		const extension = info.ext.toLowerCase()
		let nom = v.latinise(info.name.toLowerCase())
		nom = nom.replace(/\ /gi, '-')
		nom = nom.replace(/[^0-9a-z_\-]/gi, '')
		if (nom.length > 100) {
			nom = nom.substring(0, 100)
		}
		nom = nom + '_' + Math.random().toString(36).substring(2) + extension
		const chemin = path.join(__dirname, '..', '/static/fichiers/' + tableau + '/' + image)
		const destination = path.join(__dirname, '..', '/static/fichiers/' + tableau + '/' + nom)
		await fs.copy(chemin, destination, { overwrite: false, errorOnExist: false })
		res.send(nom)
	})
	
	app.post('/api/ladigitale', function (req, res) {
		const tokenApi = req.body.token
		const domaine = req.headers.host
		const lien = req.body.lien
		const params = new URLSearchParams()
		params.append('token', tokenApi)
		params.append('domaine', domaine)
		axios.post(lien, params).then(async function (resultat) {
			if (resultat.data === 'non_autorise' || resultat.data === 'erreur') {
				res.send('erreur_token')
			} else if (resultat.data === 'token_autorise' && req.body.action && req.body.action === 'creer') {
				let nom = req.body.nomUtilisateur
				if (nom === '') {
					nom = genererPseudo()
				}
				const titre = req.body.nom
				const question = req.body.question
				const reponse = req.body.reponse
				const hash = await bcrypt.hash(reponse, 10)
				const tableau = Math.random().toString(16).slice(2)
				const date = dayjs().format()
				const donnees = []
				donnees[0] = []
				const options = {}
				options.fond = '#ffffff'
				options.edition = 'ouverte'
				await db.HSET('tableaux:' + tableau, ['titre', titre, 'question', question, 'reponse', hash, 'donnees', JSON.stringify(donnees), 'options', JSON.stringify(options), 'date', date])
				const chemin = path.join(__dirname, '..', '/static/fichiers/' + tableau)
				await fs.mkdirs(chemin)
				res.send(tableau)
			} else if (resultat.data === 'token_autorise' && req.body.action && req.body.action === 'modifier-titre') {
				const tableau = req.body.id
				const titre = req.body.titre
				await db.HSET('tableaux:' + tableau, 'titre', titre)
				res.send('titre_modifie')
			} else if (resultat.data === 'token_autorise' && req.body.action && req.body.action === 'ajouter') {
				const tableau = req.body.id
				const question = req.body.question
				const reponse = req.body.reponse
				const rep = await db.EXISTS('tableaux:' + tableau)
				if (rep === null) { res.send('erreur'); return false }
				if (rep === 1) {
					let donnees = await db.HGETALL('tableaux:' + tableau)
					donnees = Object.assign({}, donnees)
					if (donnees === null) { res.send('erreur'); return false }
					if (question === donnees.question && await bcrypt.compare(reponse, donnees.reponse)) {
						res.send(donnees.titre)
					} else {
						res.send('non_autorise')
					}
				} else {
					res.send('contenu_inexistant')
				}	
			} else if (resultat.data === 'token_autorise' && req.body.action && req.body.action === 'supprimer') {
				const tableau = req.body.id
				const reponse = req.body.reponse
				const rep = await db.EXISTS('tableaux:' + tableau)
				if (rep === null) { res.send('erreur'); return false }
				if (rep === 1) {
					let donnees = await db.HGETALL('tableaux:' + tableau)
					donnees = Object.assign({}, donnees)
					if (donnees === null) { res.send('erreur'); return false }
					if (await bcrypt.compare(reponse, donnees.reponse)) {
						await db.DEL('tableaux:' + tableau)
						const chemin = path.join(__dirname, '..', '/static/fichiers/' + tableau)
						await fs.remove(chemin)
						res.send('contenu_supprime')
					} else {
						res.send('non_autorise')
					}
				} else {
					res.send('contenu_supprime')
				}
			} else {
				res.send('erreur')
			}
		}).catch(function () {
			res.send('erreur')
		})
	})
	
	app.use(function (req, res) {
		res.redirect('/')
	})

	const port = process.env.PORT || 3000
	httpServer.listen(port)

	const io = new Server(httpServer, {
		// wsEngine: eiows.Server,
		pingInterval: 95000,
    	pingTimeout: 100000,
    	maxHttpBufferSize: 1e8,
		cookie: false,
		perMessageDeflate: false
	})
	if (cluster === true) {
		io.adapter(createAdapter())
	}
	const wrap = middleware => (socket, next) => middleware(socket.request, {}, next)
	io.use(wrap(sessionMiddleware))
	
	io.on('connection', function (socket) {
		socket.on('connexion', async function (donnees) {
			const tableau = donnees.tableau
			const identifiant = donnees.identifiant
			const nom = donnees.nom
			const role = donnees.role
			const salle = 'tableau-' + tableau
			socket.data.identifiant = identifiant
			socket.data.nom = nom
			socket.data.role = role
			socket.join(salle)
			const clients = await io.in(salle).fetchSockets()
			let utilisateurs = []
			for (let i = 0; i < clients.length; i++) {
				utilisateurs.push({ identifiant: clients[i].data.identifiant, nom: clients[i].data.nom, role: clients[i].data.role })
			}
			utilisateurs = utilisateurs.filter((valeur, index, self) =>
				index === self.findIndex((t) => (
					t.identifiant === valeur.identifiant && t.nom === valeur.nom
				))
			)
			io.in(salle).emit('connexion', utilisateurs)
		})
	
		socket.on('deconnexion', function (tableau) {
			socket.leave('tableau-' + tableau)
			socket.to('tableau-' + tableau).emit('deconnexion', socket.request.session.identifiant)
		})
	
		socket.on('ajouter', async function (donnees) {
			const tableau = donnees.tableau
			if (socket.request.session.identifiant !== '' && socket.request.session.identifiant !== undefined) {
				const page = donnees.page
				const items = donnees.items
				const resultat = await db.EXISTS('tableaux:' + tableau)
				if (resultat === null) { socket.emit('erreur'); return false }
				if (resultat === 1) {
					let reponse = await db.HGETALL('tableaux:' + tableau)
					reponse = Object.assign({}, reponse)
					if (reponse === null|| !reponse.hasOwnProperty('donnees')) { socket.emit('erreur'); return false }
					const donneesTableau = JSON.parse(reponse.donnees)
					if (donneesTableau[page]) {
						donneesTableau[page].push(...items)
						await db.HSET('tableaux:' + tableau, 'donnees', JSON.stringify(donneesTableau))
						socket.to('tableau-' + tableau).emit('ajouter', { page: page, items: items })
						socket.request.session.cookie.expires = new Date(Date.now() + dureeSession)
						socket.request.session.save()
					}
				}
			}
		})

		socket.on('reajouter', async function (donnees) {
			const tableau = donnees.tableau
			if (socket.request.session.identifiant !== '' && socket.request.session.identifiant !== undefined) {
				const page = donnees.page
				const items = donnees.items
				const index = donnees.index
				const resultat = await db.EXISTS('tableaux:' + tableau)
				if (resultat === null) { socket.emit('erreur'); return false }
				if (resultat === 1) {
					let reponse = await db.HGETALL('tableaux:' + tableau)
					reponse = Object.assign({}, reponse)
					if (reponse === null || !reponse.hasOwnProperty('donnees')) { socket.emit('erreur'); return false }
					const donneesTableau = JSON.parse(reponse.donnees)
					if (donneesTableau[page]) {
						donneesTableau[page].splice(...[index, 0].concat(items))
						await db.HSET('tableaux:' + tableau, 'donnees', JSON.stringify(donneesTableau))
						socket.to('tableau-' + tableau).emit('reajouter', { page: page, items: items, index: index })
						socket.request.session.cookie.expires = new Date(Date.now() + dureeSession)
						socket.request.session.save()
					}
				}
			}
		})

		socket.on('verrouiller', async function (donnees) {
			const tableau = donnees.tableau
			if (socket.request.session.identifiant !== '' && socket.request.session.identifiant !== undefined) {
				const page = donnees.page
				const noms = donnees.noms
				const resultat = await db.EXISTS('tableaux:' + tableau)
				if (resultat === null) { socket.emit('erreur'); return false }
				if (resultat === 1) {
					let reponse = await db.HGETALL('tableaux:' + tableau)
					reponse = Object.assign({}, reponse)
					if (reponse === null || !reponse.hasOwnProperty('donnees')) { socket.emit('erreur'); return false }
					const donneesTableau = JSON.parse(reponse.donnees)
					if (donneesTableau[page]) {
						noms.forEach(function (nom) {
							donneesTableau[page].forEach(function (item) {
								if (item.name === nom) {
									item.draggable = false
									item.verrouille = true
								}
							})
						})
						await db.HSET('tableaux:' + tableau, 'donnees', JSON.stringify(donneesTableau))
						socket.to('tableau-' + tableau).emit('verrouiller', { page: page, noms: noms })
						socket.request.session.cookie.expires = new Date(Date.now() + dureeSession)
						socket.request.session.save()
					}
				}
			}
		})

		socket.on('deverrouiller', async function (donnees) {
			const tableau = donnees.tableau
			if (socket.request.session.identifiant !== '' && socket.request.session.identifiant !== undefined) {
				const page = donnees.page
				const noms = donnees.noms
				const resultat = await db.EXISTS('tableaux:' + tableau)
				if (resultat === null) { socket.emit('erreur'); return false }
				if (resultat === 1) {
					let reponse = await db.HGETALL('tableaux:' + tableau)
					reponse = Object.assign({}, reponse)
					if (reponse === null || !reponse.hasOwnProperty('donnees')) { socket.emit('erreur'); return false }
					const donneesTableau = JSON.parse(reponse.donnees)
					if (donneesTableau[page]) {
						noms.forEach(function (nom) {
							donneesTableau[page].forEach(function (item) {
								if (item.name === nom) {
									item.draggable = true
									item.verrouille = false
								}
							})
						})
						await db.HSET('tableaux:' + tableau, 'donnees', JSON.stringify(donneesTableau))
						socket.to('tableau-' + tableau).emit('deverrouiller', { page: page, noms: noms })
						socket.request.session.cookie.expires = new Date(Date.now() + dureeSession)
						socket.request.session.save()
					}
				}
			}
		})

		socket.on('modifierposition', async function (donnees) {
			const tableau = donnees.tableau
			if (socket.request.session.identifiant !== '' && socket.request.session.identifiant !== undefined) {
				const page = donnees.page
				const nom = donnees.nom
				const indexItem = donnees.index
				const resultat = await db.EXISTS('tableaux:' + tableau)
				if (resultat === null) { socket.emit('erreur'); return false }
				if (resultat === 1) {
					let reponse = await db.HGETALL('tableaux:' + tableau)
					reponse = Object.assign({}, reponse)
					if (reponse === null || !reponse.hasOwnProperty('donnees')) { socket.emit('erreur'); return false }
					const donneesTableau = JSON.parse(reponse.donnees)
					if (donneesTableau[page]) {
						donneesTableau[page].forEach(function (item, index) {
							if (item.name === nom) {
								donneesTableau[page].splice(index, 1)
								donneesTableau[page].splice(indexItem, 0, item)
							}
						})
						await db.HSET('tableaux:' + tableau, 'donnees', JSON.stringify(donneesTableau))
						socket.to('tableau-' + tableau).emit('modifierposition', { page: page, nom: nom, index: indexItem })
						socket.request.session.cookie.expires = new Date(Date.now() + dureeSession)
						socket.request.session.save()
					}
				}
			}
		})

		socket.on('deplacer', async function (donnees) {
			const tableau = donnees.tableau
			if (socket.request.session.identifiant !== '' && socket.request.session.identifiant !== undefined) {
				const page = donnees.page
				const items = donnees.items
				const resultat = await db.EXISTS('tableaux:' + tableau)
				if (resultat === null) { socket.emit('erreur'); return false }
				if (resultat === 1) {
					let reponse = await db.HGETALL('tableaux:' + tableau)
					reponse = Object.assign({}, reponse)
					if (reponse === null || !reponse.hasOwnProperty('donnees')) { socket.emit('erreur'); return false }
					const donneesTableau = JSON.parse(reponse.donnees)
					if (donneesTableau[page]) {
						items.forEach(function (obj) {
							donneesTableau[page].forEach(function (item) {
								if (item.name === obj.nom) {
									item.x = obj.x
									item.y = obj.y
									if (obj.hasOwnProperty('points')) {
										item.points = obj.points
									}
								}
							})
						})
						await db.HSET('tableaux:' + tableau, 'donnees', JSON.stringify(donneesTableau))
						socket.to('tableau-' + tableau).emit('deplacer', { page: page, items: items })
						socket.request.session.cookie.expires = new Date(Date.now() + dureeSession)
						socket.request.session.save()
					}
				}
			}
		})
	
		socket.on('redimensionner', async function (donnees) {
			const tableau = donnees.tableau
			if (socket.request.session.identifiant !== '' && socket.request.session.identifiant !== undefined) {
				const page = donnees.page
				const resultat = await db.EXISTS('tableaux:' + tableau)
				if (resultat === null) { socket.emit('erreur'); return false }
				if (resultat === 1) {
					let reponse = await db.HGETALL('tableaux:' + tableau)
					reponse = Object.assign({}, reponse)
					if (reponse === null || !reponse.hasOwnProperty('donnees')) { socket.emit('erreur'); return false }
					const donneesTableau = JSON.parse(reponse.donnees)
					if (donneesTableau[page]) {
						donneesTableau[page].forEach(function (item) {
							if (item.name === donnees.item.name) {
								item.x = donnees.item.x
								item.y = donnees.item.y
								item.width = donnees.item.width
								item.height = donnees.item.height
								item.rotation = donnees.item.rotation
								item.scaleX = donnees.item.scaleX
								item.scaleY = donnees.item.scaleY
							}
						})
						await db.HSET('tableaux:' + tableau, 'donnees', JSON.stringify(donneesTableau))
						socket.to('tableau-' + tableau).emit('redimensionner', { page: page, item: donnees.item })
						socket.request.session.cookie.expires = new Date(Date.now() + dureeSession)
						socket.request.session.save()
					}
				}
			}
		})

		socket.on('modifiertransparence', async function (donnees) {
			const tableau = donnees.tableau
			if (socket.request.session.identifiant !== '' && socket.request.session.identifiant !== undefined) {
				const page = donnees.page
				const nom = donnees.nom
				const transparence = donnees.transparence
				const resultat = await db.EXISTS('tableaux:' + tableau)
				if (resultat === null) { socket.emit('erreur'); return false }
				if (resultat === 1) {
					let reponse = await db.HGETALL('tableaux:' + tableau)
					reponse = Object.assign({}, reponse)
					if (reponse === null || !reponse.hasOwnProperty('donnees')) { socket.emit('erreur'); return false }
					const donneesTableau = JSON.parse(reponse.donnees)
					if (donneesTableau[page]) {
						donneesTableau[page].forEach(function (item) {
							if (item.name === nom) {
								item.opacity = transparence
							}
						})
						await db.HSET('tableaux:' + tableau, 'donnees', JSON.stringify(donneesTableau))
						socket.to('tableau-' + tableau).emit('modifiertransparence', { page: page, nom: nom, transparence: transparence })
						socket.request.session.cookie.expires = new Date(Date.now() + dureeSession)
						socket.request.session.save()
					}
				}
			}
		})
	
		socket.on('modifiertexte', async function (donnees) {
			const tableau = donnees.tableau
			if (socket.request.session.identifiant !== '' && socket.request.session.identifiant !== undefined) {
				const page = donnees.page
				const nom = donnees.nom
				const texte = donnees.texte
				const resultat = await db.EXISTS('tableaux:' + tableau)
				if (resultat === null) { socket.emit('erreur'); return false }
				if (resultat === 1) {
					let reponse = await db.HGETALL('tableaux:' + tableau)
					reponse = Object.assign({}, reponse)
					if (reponse === null || !reponse.hasOwnProperty('donnees')) { socket.emit('erreur'); return false }
					const donneesTableau = JSON.parse(reponse.donnees)
					if (donneesTableau[page]) {
						donneesTableau[page].forEach(function (item) {
							if (item.name === nom && item.objet === 'texte') {
								item.text = texte
							} else if (item.name === nom && item.objet !== 'texte') {
								item.text.text = texte
							}
						})
						await db.HSET('tableaux:' + tableau, 'donnees', JSON.stringify(donneesTableau))
						socket.to('tableau-' + tableau).emit('modifiertexte', { page: page, nom: nom, texte: texte })
						socket.request.session.cookie.expires = new Date(Date.now() + dureeSession)
						socket.request.session.save()
					}
				}
			}
		})
	
		socket.on('modifiertailletexte', async function (donnees) {
			const tableau = donnees.tableau
			if (socket.request.session.identifiant !== '' && socket.request.session.identifiant !== undefined) {
				const page = donnees.page
				const nom = donnees.nom
				const taille = donnees.taille
				const resultat = await db.EXISTS('tableaux:' + tableau)
				if (resultat === null) { socket.emit('erreur'); return false }
				if (resultat === 1) {
					let reponse = await db.HGETALL('tableaux:' + tableau)
					reponse = Object.assign({}, reponse)
					if (reponse === null || !reponse.hasOwnProperty('donnees')) { socket.emit('erreur'); return false }
					const donneesTableau = JSON.parse(reponse.donnees)
					if (donneesTableau[page]) {
						donneesTableau[page].forEach(function (item) {
							if (item.name === nom) {
								item.fontSize = taille
							}
						})
						await db.HSET('tableaux:' + tableau, 'donnees', JSON.stringify(donneesTableau))
						socket.to('tableau-' + tableau).emit('modifiertailletexte', { page: page, nom: nom, taille: taille })
						socket.request.session.cookie.expires = new Date(Date.now() + dureeSession)
						socket.request.session.save()
					}
				}
			}
		})
	
		socket.on('modifieralignementtexte', async function (donnees) {
			const tableau = donnees.tableau
			if (socket.request.session.identifiant !== '' && socket.request.session.identifiant !== undefined) {
				const page = donnees.page
				const nom = donnees.nom
				const alignement = donnees.alignement
				const resultat = await db.EXISTS('tableaux:' + tableau)
				if (resultat === null) { socket.emit('erreur'); return false }
				if (resultat === 1) {
					let reponse = await db.HGETALL('tableaux:' + tableau)
					reponse = Object.assign({}, reponse)
					if (reponse === null || !reponse.hasOwnProperty('donnees')) { socket.emit('erreur'); return false }
					const donneesTableau = JSON.parse(reponse.donnees)
					if (donneesTableau[page]) {
						donneesTableau[page].forEach(function (item) {
							if (item.name === nom) {
								item.align = alignement
							}
						})
						await db.HSET('tableaux:' + tableau, 'donnees', JSON.stringify(donneesTableau))
						socket.to('tableau-' + tableau).emit('modifieralignementtexte', { page: page, nom: nom, alignement: alignement })
						socket.request.session.cookie.expires = new Date(Date.now() + dureeSession)
						socket.request.session.save()
					}
				}
			}
		})

		socket.on('modifierstyletexte', async function (donnees) {
			const tableau = donnees.tableau
			if (socket.request.session.identifiant !== '' && socket.request.session.identifiant !== undefined) {
				const page = donnees.page
				const nom = donnees.nom
				const style = donnees.style
				const resultat = await db.EXISTS('tableaux:' + tableau)
				if (resultat === null) { socket.emit('erreur'); return false }
				if (resultat === 1) {
					let reponse = await db.HGETALL('tableaux:' + tableau)
					reponse = Object.assign({}, reponse)
					if (reponse === null || !reponse.hasOwnProperty('donnees')) { socket.emit('erreur'); return false }
					const donneesTableau = JSON.parse(reponse.donnees)
					if (donneesTableau[page]) {
						donneesTableau[page].forEach(function (item) {
							if (item.name === nom && item.objet === 'texte') {
								item.fontStyle = style
							} else if (item.name === nom && item.objet !== 'texte') {
								item.text.fontStyle = style
							}
						})
						await db.HSET('tableaux:' + tableau, 'donnees', JSON.stringify(donneesTableau))
						socket.to('tableau-' + tableau).emit('modifierstyletexte', { page: page, nom: nom, style: style })
						socket.request.session.cookie.expires = new Date(Date.now() + dureeSession)
						socket.request.session.save()
					}
				}
			}
		})

		socket.on('modifierdecorationtexte', async function (donnees) {
			const tableau = donnees.tableau
			if (socket.request.session.identifiant !== '' && socket.request.session.identifiant !== undefined) {
				const page = donnees.page
				const nom = donnees.nom
				const decoration = donnees.decoration
				const resultat = await db.EXISTS('tableaux:' + tableau)
				if (resultat === null) { socket.emit('erreur'); return false }
				if (resultat === 1) {
					let reponse = await db.HGETALL('tableaux:' + tableau)
					reponse = Object.assign({}, reponse)
					if (reponse === null || !reponse.hasOwnProperty('donnees')) { socket.emit('erreur'); return false }
					const donneesTableau = JSON.parse(reponse.donnees)
					if (donneesTableau[page]) {
						donneesTableau[page].forEach(function (item) {
							if (item.name === nom && item.objet === 'texte') {
								item.textDecoration = decoration
							} else if (item.name === nom && item.objet !== 'texte') {
								item.text.textDecoration = decoration
							}
						})
						await db.HSET('tableaux:' + tableau, 'donnees', JSON.stringify(donneesTableau))
						socket.to('tableau-' + tableau).emit('modifierdecorationtexte', { page: page, nom: nom, decoration: decoration })
						socket.request.session.cookie.expires = new Date(Date.now() + dureeSession)
						socket.request.session.save()
					}
				}
			}
		})
	
		socket.on('modifierfiltre', async function (donnees) {
			const tableau = donnees.tableau
			if (socket.request.session.identifiant !== '' && socket.request.session.identifiant !== undefined) {
				const page = donnees.page
				const nom = donnees.nom
				const filtre = donnees.filtre
				const resultat = await db.EXISTS('tableaux:' + tableau)
				if (resultat === null) { socket.emit('erreur'); return false }
				if (resultat === 1) {
					let reponse = await db.HGETALL('tableaux:' + tableau)
					reponse = Object.assign({}, reponse)
					if (reponse === null || !reponse.hasOwnProperty('donnees')) { socket.emit('erreur'); return false }
					const donneesTableau = JSON.parse(reponse.donnees)
					if (donneesTableau[page]) {
						donneesTableau[page].forEach(function (item) {
							if (item.name === nom) {
								item.filtre = filtre
							}
						})
						await db.HSET('tableaux:' + tableau, 'donnees', JSON.stringify(donneesTableau))
						socket.to('tableau-' + tableau).emit('modifierfiltre', { page: page, nom: nom, filtre: filtre })
						socket.request.session.cookie.expires = new Date(Date.now() + dureeSession)
						socket.request.session.save()
					}
				}
			}
		})
	
		socket.on('modifiercouleur', async function (donnees) {
			const tableau = donnees.tableau
			if (socket.request.session.identifiant !== '' && socket.request.session.identifiant !== undefined) {
				const page = donnees.page
				const nom = donnees.nom
				const couleur = donnees.couleur
				const resultat = await db.EXISTS('tableaux:' + tableau)
				if (resultat === null) { socket.emit('erreur'); return false }
				if (resultat === 1) {
					let reponse = await db.HGETALL('tableaux:' + tableau)
					reponse = Object.assign({}, reponse)
					if (reponse === null || !reponse.hasOwnProperty('donnees')) { socket.emit('erreur'); return false }
					const donneesTableau = JSON.parse(reponse.donnees)
					if (donneesTableau[page]) {
						donneesTableau[page].forEach(function (item) {
							if (item.name === nom && (item.objet === 'rectangle' || item.objet === 'cercle' || item.objet === 'dessin')) {
								item.stroke = couleur
							} else if (item.name === nom && (item.objet === 'rectangle-plein' || item.objet === 'cercle-plein' || item.objet === 'etoile' || item.objet === 'surlignage' || item.objet === 'texte')) {
								item.fill = couleur
							} else if (item.name === nom && (item.objet === 'ligne' || item.objet === 'fleche' || item.objet === 'ancre')) {
								item.stroke = couleur
								item.fill = couleur
							} else if (item.name === nom && item.objet === 'label') {
								item.tag.fill = couleur
							}
						})
						await db.HSET('tableaux:' + tableau, 'donnees', JSON.stringify(donneesTableau))
						socket.to('tableau-' + tableau).emit('modifiercouleur', { page: page, nom: nom, couleur: couleur })
						socket.request.session.cookie.expires = new Date(Date.now() + dureeSession)
						socket.request.session.save()
					}
				}
			}
		})
	
		socket.on('modifierfond', function (donnees) {
			const tableau = donnees.tableau
			if (socket.request.session.identifiant !== '' && socket.request.session.identifiant !== undefined) {
				socket.to('tableau-' + tableau).emit('modifierfond', { fond: donnees.fond })
				socket.request.session.cookie.expires = new Date(Date.now() + dureeSession)
				socket.request.session.save()
			}
		})
	
		socket.on('ajouterpage', async function (donnees) {
			const tableau = donnees.tableau
			if (socket.request.session.identifiant !== '' && socket.request.session.identifiant !== undefined) {
				const resultat = await db.EXISTS('tableaux:' + tableau)
				if (resultat === null) { socket.emit('erreur'); return false }
				if (resultat === 1) {
					let reponse = await db.HGETALL('tableaux:' + tableau)
					reponse = Object.assign({}, reponse)
					if (reponse === null) { socket.emit('erreur'); return false }
					const donneesTableau = JSON.parse(reponse.donnees)
					donneesTableau.splice(donnees.page, 0, [])
					await db.HSET('tableaux:' + tableau, 'donnees', JSON.stringify(donneesTableau))
					socket.to('tableau-' + tableau).emit('ajouterpage')
					socket.request.session.cookie.expires = new Date(Date.now() + dureeSession)
					socket.request.session.save()
				}
			}
		})
	
		socket.on('reajouterpage', async function (donnees) {
			const tableau = donnees.tableau
			if (socket.request.session.identifiant !== '' && socket.request.session.identifiant !== undefined) {
				const page = donnees.page
				const items = donnees.items
				const resultat = await db.EXISTS('tableaux:' + tableau)
				if (resultat === null) { socket.emit('erreur'); return false }
				if (resultat === 1) {
					let reponse = await db.HGETALL('tableaux:' + tableau)
					reponse = Object.assign({}, reponse)
					if (reponse === null) { socket.emit('erreur'); return false }
					const donneesTableau = JSON.parse(reponse.donnees)
					donneesTableau.splice(page, 0, items)
					await db.HSET('tableaux:' + tableau, 'donnees', JSON.stringify(donneesTableau))
					socket.to('tableau-' + tableau).emit('reajouterpage', { page: page, items: items })
					socket.request.session.cookie.expires = new Date(Date.now() + dureeSession)
					socket.request.session.save()
				}
			}
		})
	
		socket.on('deplacerpage', async function (donnees) {
			const tableau = donnees.tableau
			if (socket.request.session.identifiant !== '' && socket.request.session.identifiant !== undefined) {
				const resultat = await db.EXISTS('tableaux:' + tableau)
				if (resultat === null) { socket.emit('erreur'); return false }
				if (resultat === 1) {
					const ancienIndex = donnees.ancienIndex
					const nouvelIndex = donnees.nouvelIndex
					let reponse = await db.HGETALL('tableaux:' + tableau)
					reponse = Object.assign({}, reponse)
					if (reponse === null) { socket.emit('erreur'); return false }
					const donneesTableau = JSON.parse(reponse.donnees)
					const donneesPage = donneesTableau.splice(ancienIndex, 1)[0]
					donneesTableau.splice(nouvelIndex, 0, donneesPage)
					await db.HSET('tableaux:' + tableau, 'donnees', JSON.stringify(donneesTableau))
					socket.to('tableau-' + tableau).emit('deplacerpage', { ancienIndex: ancienIndex, nouvelIndex: nouvelIndex })
					socket.request.session.cookie.expires = new Date(Date.now() + dureeSession)
					socket.request.session.save()
				}
			}
		})
	
		socket.on('supprimerpage', async function (donnees) {
			const tableau = donnees.tableau
			if (socket.request.session.identifiant !== '' && socket.request.session.identifiant !== undefined) {
				const resultat = await db.EXISTS('tableaux:' + tableau)
				if (resultat === null) { socket.emit('erreur'); return false }
				if (resultat === 1) {
					let reponse = await db.HGETALL('tableaux:' + tableau)
					reponse = Object.assign({}, reponse)
					if (reponse === null) { socket.emit('erreur'); return false }
					const donneesTableau = JSON.parse(reponse.donnees)
					donneesTableau.splice(donnees.page, 1)
					await db.HSET('tableaux:' + tableau, 'donnees', JSON.stringify(donneesTableau))
					socket.to('tableau-' + tableau).emit('supprimerpage', { page: donnees.page })
					socket.request.session.cookie.expires = new Date(Date.now() + dureeSession)
					socket.request.session.save()
				}
			}
		})
	
		socket.on('importer', function (donnees) {
			const tableau = donnees.tableau
			if (socket.request.session.identifiant !== '' && socket.request.session.identifiant !== undefined) {
				socket.to('tableau-' + tableau).emit('importer', { items: donnees.items, commentaires: donnees.commentaires, fond: donnees.fond })
				socket.request.session.cookie.expires = new Date(Date.now() + dureeSession)
				socket.request.session.save()
			}
		})
	
		socket.on('supprimer', async function (donnees) {
			const tableau = donnees.tableau
			if (socket.request.session.identifiant !== '' && socket.request.session.identifiant !== undefined) {
				const page = donnees.page
				const nom = donnees.nom
				const resultat = await db.EXISTS('tableaux:' + tableau)
				if (resultat === null) { socket.emit('erreur'); return false }
				if (resultat === 1) {
					let reponse = await db.HGETALL('tableaux:' + tableau)
					reponse = Object.assign({}, reponse)
					if (reponse === null || !reponse.hasOwnProperty('donnees')) { socket.emit('erreur'); return false }
					const donneesTableau = JSON.parse(reponse.donnees)
					if (donneesTableau[page]) {
						donneesTableau[page].forEach(function (item, index) {
							if (item.name.includes(nom)) {
								donneesTableau[page].splice(index, 1)
							}
						})
						await db.HSET('tableaux:' + tableau, 'donnees', JSON.stringify(donneesTableau))
						socket.to('tableau-' + tableau).emit('supprimer', { page: page, nom: nom })
						socket.request.session.cookie.expires = new Date(Date.now() + dureeSession)
						socket.request.session.save()
					}
				}
			}
		})
	
		socket.on('supprimerselection', async function (donnees) {
			const tableau = donnees.tableau
			if (socket.request.session.identifiant !== '' && socket.request.session.identifiant !== undefined) {
				const page = donnees.page
				const noms = donnees.noms
				const resultat = await db.EXISTS('tableaux:' + tableau)
				if (resultat === null) { socket.emit('erreur'); return false }
				if (resultat === 1) {
					let reponse = await db.HGETALL('tableaux:' + tableau)
					reponse = Object.assign({}, reponse)
					if (reponse === null || !reponse.hasOwnProperty('donnees')) { socket.emit('erreur'); return false }
					const donneesTableau = JSON.parse(reponse.donnees)
					if (donneesTableau[page]) {
						for (let i = 0; i < donneesTableau[page].length; i++) {
							for (let j = 0; j < noms.length; j++) {
								if (donneesTableau[page][i] && donneesTableau[page][i].hasOwnProperty('name') && donneesTableau[page][i].name.includes(noms[j])) {
									donneesTableau[page].splice(i, 1)
								}
							}
						}
						await db.HSET('tableaux:' + tableau, 'donnees', JSON.stringify(donneesTableau))
						socket.to('tableau-' + tableau).emit('supprimerselection', { page: page, noms: noms })
						socket.request.session.cookie.expires = new Date(Date.now() + dureeSession)
						socket.request.session.save()
					}
				}
			}
		})

		socket.on('enregistrercommentaire', async function (donnees) {
			const tableau = donnees.tableau
			if (socket.request.session.identifiant !== '' && socket.request.session.identifiant !== undefined) {
				const itemId = donnees.itemId
				const commentaire = donnees.commentaire
				const date = dayjs().format()
				commentaire.date = date
				const resultat = await db.EXISTS('tableaux:' + tableau)
				if (resultat === null) { socket.emit('erreur'); return false }
				if (resultat === 1) {
					let reponse = await db.HGETALL('tableaux:' + tableau)
					reponse = Object.assign({}, reponse)
					if (reponse === null) { socket.emit('erreur'); return false }
					let donneesCommentaires = {}
					if (reponse.hasOwnProperty('commentaires') === true) {
						donneesCommentaires = JSON.parse(reponse.commentaires)
					}
					if (!donneesCommentaires.hasOwnProperty(itemId)) {
						donneesCommentaires[itemId] = []
					}
					donneesCommentaires[itemId].push(commentaire)
					await db.HSET('tableaux:' + tableau, 'commentaires', JSON.stringify(donneesCommentaires))
					io.in('tableau-' + tableau).emit('enregistrercommentaire', { itemId: itemId, commentaire: commentaire, page: donnees.page })
					socket.request.session.cookie.expires = new Date(Date.now() + dureeSession)
					socket.request.session.save()
				}
			}
		})

		socket.on('enregistrercommentairemodifie', async function (donnees) {
			const tableau = donnees.tableau
			if (socket.request.session.identifiant !== '' && socket.request.session.identifiant !== undefined) {
				const itemId = donnees.itemId
				const commentaire = donnees.commentaire
				const resultat = await db.EXISTS('tableaux:' + tableau)
				if (resultat === null) { socket.emit('erreur'); return false }
				if (resultat === 1) {
					let reponse = await db.HGETALL('tableaux:' + tableau)
					reponse = Object.assign({}, reponse)
					if (reponse === null) { socket.emit('erreur'); return false }
					const donneesCommentaires = JSON.parse(reponse.commentaires)
					donneesCommentaires[itemId].forEach(function (commentaire) {
						if (commentaire.id === donnees.commentaire.id) {
							commentaire.texte = donnees.commentaire.texte
						}
					})
					await db.HSET('tableaux:' + tableau, 'commentaires', JSON.stringify(donneesCommentaires))
					io.in('tableau-' + tableau).emit('enregistrercommentairemodifie', { itemId: itemId, commentaire: commentaire })
					socket.request.session.cookie.expires = new Date(Date.now() + dureeSession)
					socket.request.session.save()
				}
			}
		})

		socket.on('supprimercommentaire', async function (donnees) {
			const tableau = donnees.tableau
			if (socket.request.session.identifiant !== '' && socket.request.session.identifiant !== undefined) {
				const itemId = donnees.itemId
				const commentaireId = donnees.commentaireId
				const resultat = await db.EXISTS('tableaux:' + tableau)
				if (resultat === null) { socket.emit('erreur'); return false }
				if (resultat === 1) {
					let reponse = await db.HGETALL('tableaux:' + tableau)
					reponse = Object.assign({}, reponse)
					if (reponse === null) { socket.emit('erreur'); return false }
					const donneesCommentaires = JSON.parse(reponse.commentaires)
					donneesCommentaires[itemId].forEach(function (commentaire, index) {
						if (commentaire.id === commentaireId) {
							donneesCommentaires[itemId].splice(index, 1)
						}
					})
					await db.HSET('tableaux:' + tableau, 'commentaires', JSON.stringify(donneesCommentaires))
					io.in('tableau-' + tableau).emit('supprimercommentaire', { itemId: itemId, commentaireId: commentaireId })
					socket.request.session.cookie.expires = new Date(Date.now() + dureeSession)
					socket.request.session.save()
				}
			}
		})

		socket.on('supprimercommentaires', async function (donnees) {
			const tableau = donnees.tableau
			if (socket.request.session.identifiant !== '' && socket.request.session.identifiant !== undefined) {
				const itemId = donnees.itemId
				const resultat = await db.EXISTS('tableaux:' + tableau)
				if (resultat === null) { socket.emit('erreur'); return false }
				if (resultat === 1) {
					let reponse = await db.HGETALL('tableaux:' + tableau)
					reponse = Object.assign({}, reponse)
					if (reponse === null) { socket.emit('erreur'); return false }
					const donneesCommentaires = JSON.parse(reponse.commentaires)
					if (donneesCommentaires.hasOwnProperty(itemId) === true) {
						delete donneesCommentaires[itemId]
					}
					await db.HSET('tableaux:' + tableau, 'commentaires', JSON.stringify(donneesCommentaires))
					io.in('tableau-' + tableau).emit('supprimercommentaires', { itemId: itemId, page: donnees.page })
					socket.request.session.cookie.expires = new Date(Date.now() + dureeSession)
					socket.request.session.save()
				}
			}
		})
	
		socket.on('modifierroleutilisateur', function (donnees) {
			const tableau = donnees.tableau
			if (socket.request.session.identifiant !== '' && socket.request.session.identifiant !== undefined && socket.request.session.role === 'auteur' && socket.request.session.tableaux.includes(tableau)) {
				if (socket.request.session.identifiant === donnees.identifiant) {
					socket.request.session.role = donnees.role
				} else {
					io.in('tableau-' + tableau).emit('modifierroleutilisateur', { admin: donnees.admin, identifiant: donnees.identifiant, role: donnees.role })
				}
				socket.request.session.cookie.expires = new Date(Date.now() + dureeSession)
				socket.request.session.save()
			}
		})

		socket.on('attribuerroleparticipant', function (donnees) {
			if (socket.request.session.identifiant !== '' && socket.request.session.identifiant !== undefined && socket.request.session.identifiant === donnees.identifiant) {
				socket.request.session.role = 'participant'
				socket.emit('attribuerroleparticipant', donnees.identifiant)
				socket.request.session.cookie.expires = new Date(Date.now() + dureeSession)
				socket.request.session.save()
			}
		})
	
		socket.on('modifierlangue', function (langue) {
			socket.request.session.langue = langue
			socket.request.session.save()
		})
	
		socket.on('modifieredition', async function (donnees) {
			const tableau = donnees.tableau
			const resultat = await db.EXISTS('tableaux:' + tableau)
			if (resultat === null) { socket.emit('erreur'); return false }
			if (resultat === 1) {
				let reponse = await db.HGETALL('tableaux:' + tableau)
				reponse = Object.assign({}, reponse)
				if (reponse === null) { socket.emit('erreur'); return false }
				const options = JSON.parse(reponse.options)
				options.edition = donnees.edition
				await db.HSET('tableaux:' + tableau, 'options', JSON.stringify(options))
				io.in('tableau-' + tableau).emit('modifieredition', donnees.edition)
				socket.request.session.cookie.expires = new Date(Date.now() + dureeSession)
				socket.request.session.save()
			}
		})
	
		socket.on('modifiermultipage', async function (donnees) {
			const tableau = donnees.tableau
			const resultat = await db.EXISTS('tableaux:' + tableau)
			if (resultat === null) { socket.emit('erreur'); return false }
			if (resultat === 1) {
				let reponse = await db.HGETALL('tableaux:' + tableau)
				reponse = Object.assign({}, reponse)
				if (reponse === null) { socket.emit('erreur'); return false }
				const options = JSON.parse(reponse.options)
				options.multipage = donnees.multipage
				await db.HSET('tableaux:' + tableau, 'options', JSON.stringify(options))
				io.in('tableau-' + tableau).emit('modifiermultipage', donnees.multipage)
				socket.request.session.cookie.expires = new Date(Date.now() + dureeSession)
				socket.request.session.save()
			}
		})

		socket.on('modifieraffichageauteur', async function (donnees) {
			const tableau = donnees.tableau
			const resultat = await db.EXISTS('tableaux:' + tableau)
			if (resultat === null) { socket.emit('erreur'); return false }
			if (resultat === 1) {
				let reponse = await db.HGETALL('tableaux:' + tableau)
				reponse = Object.assign({}, reponse)
				if (reponse === null) { socket.emit('erreur'); return false }
				const options = JSON.parse(reponse.options)
				options.affichageAuteur = donnees.affichageAuteur
				await db.HSET('tableaux:' + tableau, 'options', JSON.stringify(options))
				io.in('tableau-' + tableau).emit('modifieraffichageauteur', donnees.affichageAuteur)
				socket.request.session.cookie.expires = new Date(Date.now() + dureeSession)
				socket.request.session.save()
			}
		})
	
		socket.on('reinitialiser', async function (donnees) {
			const tableau = donnees.tableau
			if (socket.request.session.identifiant !== '' && socket.request.session.identifiant !== undefined && socket.request.session.role === 'auteur' && socket.request.session.tableaux.includes(tableau)) {
				const resultat = await db.EXISTS('tableaux:' + tableau)
				if (resultat === null) { socket.emit('erreur'); return false }
				if (resultat === 1) {
					await db.HSET('tableaux:' + tableau, 'donnees', JSON.stringify([[]]), 'commentaires', JSON.stringify({}))
					io.in('tableau-' + tableau).emit('reinitialiser')
					socket.request.session.cookie.expires = new Date(Date.now() + dureeSession)
					socket.request.session.save()
				}
			}
		})
	
		socket.on('supprimerimages', async function (donnees) {
			for (let i = 0; i < donnees.images.length; i++) {
				const chemin = path.join(__dirname, '..', '/static/fichiers/' + donnees.tableau + '/' + donnees.images[i])
				await fs.remove(chemin)
			}
		})
	})

	async function verifierAcces (req, tableau, question, reponse) {
		return new Promise(async function (resolve) {
			let resultat = await db.HGETALL('tableaux:' + tableau)
			resultat = Object.assign({}, resultat)
			if (resultat === null || !resultat.hasOwnProperty('question') || !resultat.hasOwnProperty('reponse')) { resolve('erreur'); return false }
			if (resultat.question === question && await bcrypt.compare(reponse, resultat.reponse)) {
				req.session.role = 'auteur'
				if (!req.session.hasOwnProperty('tableaux')) {
					req.session.tableaux = []
				}
				if (!req.session.tableaux.includes(tableau)) {
					req.session.tableaux.push(tableau)
				}
				if (!req.session.hasOwnProperty('digidrive')) {
					req.session.digidrive = []
				}
				req.session.digidrive.push(tableau)
				resolve('tableau_debloque')
			} else {
				resolve('erreur')
			}
		})
	}

	function genererPseudo () {
		const caracteres = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
		const nom = caracteres.charAt(Math.floor(Math.random() * caracteres.length)) + caracteres.charAt(Math.floor(Math.random() * caracteres.length)) + Math.floor(Math.random() * (9999 - 1000) + 1000)
		return nom
	}
	
	const televerser = multer({
		storage: multer.diskStorage({
			destination: function (req, fichier, callback) {
				const tableau = req.body.tableau
				const chemin = path.join(__dirname, '..', '/static/fichiers/' + tableau + '/')
				callback(null, chemin)
			},
			filename: function (req, fichier, callback) {
				const info = path.parse(fichier.originalname)
				const extension = info.ext.toLowerCase()
				let nom = v.latinise(info.name.toLowerCase())
				nom = nom.replace(/\ /gi, '-')
				nom = nom.replace(/[^0-9a-z_\-]/gi, '')
				if (nom.length > 100) {
					nom = nom.substring(0, 100)
				}
				nom = nom + '_' + Math.random().toString(36).substring(2) + extension
				callback(null, nom)
			}
		})
	}).single('fichier')
	
	const televerserArchive = multer({
		storage: multer.diskStorage({
			destination: function (req, fichier, callback) {
				const chemin = path.join(__dirname, '..', '/static/temp/')
				callback(null, chemin)
			},
			filename: function (req, fichier, callback) {
				const info = path.parse(fichier.originalname)
				const extension = info.ext.toLowerCase()
				let nom = v.latinise(info.name.toLowerCase())
				nom = nom.replace(/\ /gi, '-')
				nom = nom.replace(/[^0-9a-z_\-]/gi, '')
				if (nom.length > 100) {
					nom = nom.substring(0, 100)
				}
				nom = nom + '_' + Math.random().toString(36).substring(2) + extension
				callback(null, nom)
			}
		})
	}).single('fichier')	
}
